

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

public class FileCopy {
    public static void main(String[] args) {
        try {
            System.out.println(args[0]);
            File source = new File(args[0]);
            File dest = new File(args[1]);
            Files.copy(source.toPath(), dest.toPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
