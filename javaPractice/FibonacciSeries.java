
import java.util.Scanner;
public class FibonacciSeries {
    public static void main(String[] args) {
        try {
            Long userInput, a = 0l, b = 0l, c = 1l;
            Scanner scannerObj = new Scanner(System.in);
            System.out.print("Enter series length: ");
            userInput = scannerObj.nextLong();
            if (userInput <= 0) throw new Exception("please enter a number greater than 0");
            System.out.print("Series: ");
            for (int i = 1; i <= userInput; i++) {
                a = b;
                b = c;
                c = a + b;
                System.out.print((Long)a + " ");
            }
        } catch (Exception e) {
            String errorMsg = e.getMessage();
            if (errorMsg == null) errorMsg = "Please enter a Valid Number";
            System.out.println(errorMsg);
        }
        System.out.println(4660046610375530309l+7540113804746346429l);

    }
}
