package com.graphikos.java.practice;

import java.io.*;

public class FileReader {
    public static void main(String args[]) {
        try {
            File file = new File("test.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String line;
            int lineCount = 1;
            while ((line = br.readLine()) != null) {
                sb.append(lineCount + ".");
                sb.append(line);
                sb.append("\n");
                lineCount++;
            }
            System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}