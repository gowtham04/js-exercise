

import java.util.Arrays;

public class ThreeDimensionalArray {
    public static void main(String args[]) {
        int rows = 5;
        int columns = 8;
        int arrayCount = 4;
        int arr[][][] = new int[arrayCount][rows][columns];
        int i, j, k;
        int maxVal = Integer.MIN_VALUE;
        int minVal = Integer.MAX_VALUE;
        for (i = 0; i < arrayCount; i++) {
            for (j = 0; j < rows; j++) {
                for (k = 0; k < columns; k++) {
//                  let ran
                    int random_int = (int) Math.floor(Math.random() * (Integer.MAX_VALUE));
                    if (random_int > maxVal) maxVal = random_int;
                    if (random_int < minVal) minVal = random_int;
                    arr[i][j][k] = random_int;

                }
            }
        }
        String outPutStr = "[ \n";
        for (i = 0; i < arrayCount; i++) {
            outPutStr += "[";
            for (j = 0; j < rows; j++) {
                outPutStr += (j != rows - 1) ? Arrays.toString(arr[i][j]) + "," : Arrays.toString(arr[i][j]);
            }
            outPutStr += (i != arrayCount - 1) ? "], \n" : "] \n";
        }
        outPutStr += "]";
        System.out.println(outPutStr);
        System.out.println("MAX: " + maxVal);
        System.out.println("MIN: " + minVal);
    }
}
