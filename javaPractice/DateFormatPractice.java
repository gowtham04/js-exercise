

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatPractice {
    public static void main(String[] args) {
        String pattern = "EEE MMM dd HH:mm:ss zzz yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        System.out.println(date);

    }
}
