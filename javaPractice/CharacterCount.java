

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CharacterCount {
    public static void main(String[] args) throws IOException {
        String userInput;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            while(true){
                System.out.print("Enter something:");
                userInput = br.readLine();
                System.out.println("Number Of Characters in : "+userInput +" "+ userInput.length());
            }
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }
}
