
import java.io.*;
public class DataInputStreamPractice {
    public static void main(String[] args) throws IOException {
        DataInputStream dis = new DataInputStream(System.in);
        System.out.print("Please give your input: ");
        String str1 = dis.readLine();
        System.out.println("The input given by user: " + str1);
    }
}
