class Slider {
  constructor(parentElement, points) {
    this.parentElement = parentElement;
    this.dragged = false;
    this.points = points;
    this.createElements();
  }
  createElements() {
    //Creating Elements
    let wrapper = document.createElement("div");
    let leftControls = document.createElement("div");
    let controlWrapper = document.createElement("div");
    let controlBg = document.createElement("div");
    let controller = document.createElement("div");
    let rightControls = document.createElement("div");
    let leftMost = document.createElement("i");
    let rightMost = document.createElement("i");
    let moveLeft = document.createElement("i");
    let moveRight = document.createElement("i");
    controller.textContent = 0;
    wrapper.style.width = this.points + 200 + "px";
    controlWrapper.style.width = this.points + 50 + "px";

    // Adding classes;
    {
      wrapper.classList = "wrapper";
      leftControls.classList = "leftControls";
      controlWrapper.classList = "controlWrapper";
      controlBg.classList = "controlBg";
      controller.classList = "control";
      rightControls.classList = "rightControls";
      leftMost.classList = "fa fa-step-backward leftMost";
      rightMost.classList = "fa fa-step-forward rightMost";
      moveLeft.classList = "fa fa-caret-left moveLeft";
      moveRight.classList = "fa fa-caret-right moveRight";
    }
    //appending Child elements
    {
      leftControls.appendChild(leftMost);
      leftControls.appendChild(moveLeft);
      rightControls.appendChild(moveRight);
      rightControls.appendChild(rightMost);
      controlWrapper.appendChild(controlBg);
      controlWrapper.appendChild(controller);
      wrapper.appendChild(leftControls);
      wrapper.appendChild(controlWrapper);
      wrapper.appendChild(rightControls);
      this.parentElement.appendChild(wrapper);
    }

    this.slider = wrapper;
    this.addEventListeners();
  }
  addEventListeners() {
    let slider = this.slider;
    let controlWrapper = slider.querySelector(".controlWrapper");
    let control = slider.querySelector(".control");
    let leftMost = slider.querySelector(".leftMost");
    let rightMost = slider.querySelector(".rightMost");
    let moveLeft = slider.querySelector(".moveLeft");
    let moveRight = slider.querySelector(".moveRight");

    control.addEventListener(
      "mousedown",
      function () {
        this.dragged = true;
        control.classList.toggle("active");
      }.bind(this)
    );
    this.parentElement.addEventListener(
      "mouseup",
      function () {
        this.dragged = false;
        controlWrapper.classList.toggle("active");
      }.bind(this)
    );
    controlWrapper.addEventListener(
      "click",
      function () {
        this.dragged = true;
        this.moveControl(event);
        this.dragged = false;
      }.bind(this)
    );

    document.addEventListener(
      "mousemove",
      function () {
        this.moveControl(event);
      }.bind(this)
    );
    rightMost.addEventListener(
      "click",
      function () {
        this.moveControl(controlWrapper.clientWidth);
      }.bind(this)
    );
    leftMost.addEventListener(
      "click",
      function () {
        this.moveControl(0);
      }.bind(this)
    );
    moveLeft.addEventListener(
      "click",
      function () {
        this.moveControl(control.offsetLeft - 1);
      }.bind(this)
    );
    moveRight.addEventListener(
      "click",
      function () {
        this.moveControl(control.offsetLeft + 1);
      }.bind(this)
    );
  }

  moveControl(event) {
    let controlWrapper = this.slider.querySelector(".controlWrapper");
    let controlBg = controlWrapper.querySelector(".controlBg");
    let controller = controlWrapper.querySelector(".control");
    let maxWidth = controlWrapper.clientWidth;
    let controllerWidth = controller.clientWidth;
    let val = event.type
      ? event.clientX - controlWrapper.offsetLeft - controllerWidth
      : event;
    if (this.dragged || !event.type) {
      if (val < 0) val = 0;
      else if (val > maxWidth - controllerWidth)
        val = maxWidth - controllerWidth;
      controller.style.left = val + "px";
      controller.textContent = val;
      controlBg.style.width = val + 5 + "px";
    }
  }
}
document.querySelector(".buttons").addEventListener("click", createSlider);
function createSlider() {
  let currentEleData = event.target.dataset;
  if (currentEleData) {
    if (currentEleData.points > 1500) {
      alert("please give less than 1500 points");
    }
    let parentElement = document.querySelector(currentEleData.parent);
    new Slider(parentElement, Number(currentEleData.points));
  }
}
