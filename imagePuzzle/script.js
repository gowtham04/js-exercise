Array.prototype.shuffle = function () {
  let array = this;
  let currentIndex = array.length - 1,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};

class Puzzle {
  constructor(imgSrc, container) {
    this.img = imgSrc;
    this.container = container;
    this.moves = 0;
    this.createElements();
  }
  createElements() {
    let puzzleWrapper = document.createElement("div");
    puzzleWrapper.classList.add("puzzleWrapper");
    let puzzlePreviewContainer = document.createElement("div");
    let puzzle = document.createElement("div");
    let preview = document.createElement("div");
    preview.classList = "preview";
    preview.style.backgroundImage = `url(${this.img})`;
    puzzle.classList = "puzzle";
    let moves = document.createElement("div");
    moves.classList = "moves";
    moves.textContent = `MOVES ${this.moves}`;
    let tiles = [];
    for (let i = 0; i < 9; i++) {
      let tile = document.createElement("div");
      if (i !== 8) {
        tile.classList = `tile${i} tile `;
        tile.style.backgroundImage = `url(${this.img})`;
      } else tile.classList = `tile${i} tile active`;
      tiles.push(tile);
    }
    tiles.shuffle();
    {
      let row = 1;
      let column = 1;
      for (let tile of tiles) {
        tile.dataset.row = row;
        tile.dataset.column = column;
        column++;
        if (column == 4) {
          row++;
          column = 1;
        }
      }
    }
    tiles.map((tile) => puzzle.appendChild(tile));
    this.puzzle = puzzleWrapper;
    this.addEventHandlers();

    puzzlePreviewContainer.appendChild(preview);
    puzzlePreviewContainer.appendChild(puzzle);
    puzzleWrapper.appendChild(moves);
    puzzleWrapper.appendChild(puzzlePreviewContainer);
    this.container.appendChild(puzzleWrapper);
  }
  addEventHandlers() {
    let puzzle = this.puzzle;
    puzzle.addEventListener(
      "click",
      function () {
        let currentEle = event.target;
        if (currentEle.classList.contains("active")) return;
        let activeDiv = puzzle.querySelector(".active");
        let activeRow = Number(activeDiv.dataset.row);
        let activeCol = Number(activeDiv.dataset.column);
        let curRow = Number(currentEle.dataset.row);
        let curCol = Number(currentEle.dataset.column);
        if (
          (curCol == activeCol && Math.abs(curRow - activeRow) <= 1) ||
          (curRow == activeRow && Math.abs(curCol - activeCol) <= 1)
        ) {
          this.moves++;
          this.puzzle.querySelector(
            ".moves"
          ).textContent = `MOVES ${this.moves}`;
          activeDiv.classList = `${currentEle.classList[0]} tile`;
          activeDiv.style.backgroundImage = `url(${this.img})`;
          currentEle.classList = `tile8 tile active`;
          currentEle.style.backgroundImage = "none";
        }
        this.checkFinish();
      }.bind(this)
    );
  }
  checkFinish() {
    let tiles = this.puzzle.querySelectorAll(".tile");
    for (let i = 0; i < 9; i++) {
      if (i != tiles[i].classList[0].substring(4)) {
        return false;
      }
    }
    this.puzzle.querySelector(".tile8").classList.toggle("active");
    this.puzzle.querySelector(
      ".tile8"
    ).style.backgroundImage = `url(${this.img})`;
  }
}

new Puzzle("puzzleImage.jpg", document.querySelector(".wrapper"));
