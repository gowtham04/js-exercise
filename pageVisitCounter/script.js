(function getCookie(cname) {
  let currentCookie = document.cookie.split("count=");
  let count;
  if (currentCookie.length != 2) {
    document.cookie = "count=0";
    count = 1;
  } else {
    count = Number(currentCookie[1]) + 1;
  }
  document.cookie = "count" + "=" + count + ";";
  document.querySelector("#count").textContent = count;
})();
