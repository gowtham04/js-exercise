let getclientDetails = getBrowsersDetails();
(function renderData() {
  let clientDetails = getclientDetails();
  let tableEle = document.createElement("table");
  tableEle.classList.add(...["table", "table-striped"]);
  Object.entries(clientDetails).forEach(function (ele) {
    let rowEle = document.createElement("tr");
    let propertyEle = document.createElement("td");
    let valueEle = document.createElement("td");
    let [property, value] = ele;
    valueEle.setAttribute("id", property);
    propertyEle.textContent = property;
    valueEle.textContent = value;
    rowEle.appendChild(propertyEle);
    rowEle.appendChild(valueEle);
    tableEle.appendChild(rowEle);
  });
  document.querySelector("#wrapper").appendChild(tableEle);
})();

function getBrowsersDetails() {
  let userAgent = navigator.userAgent;
  let name, version;
  if (userAgent.indexOf("OPR") != -1) {
    name = "Opera";
    version = userAgent.split("OPR/")[1].split(" ")[0];
  } else if (userAgent.indexOf("Edg") != -1) {
    name = "Edge";
    version = userAgent.split("Edg/")[1].split(" ")[0];
  } else if (userAgent.indexOf("Chrome") != -1) {
    name = "Chrome";
    version = userAgent.split("Chrome/")[1].split(" ")[0];
  } else if (userAgent.indexOf("Safari") != -1) {
    name = "Safari";
    version = userAgent.split("Version/")[1].split(" ")[0];
  } else if (userAgent.indexOf("Firefox") != -1) {
    name = "Firefox";
    version = userAgent.split("Firefox/")[1].split(" ")[0];
  } else if (userAgent.indexOf("MSIE") != -1 || document.documentMode) {
    name = "Internet Explorer";
    version = userAgent.split("rv:")[1].split(")")[0];
  } else {
    name = "unknown";
    version = "unknown";
  }
  function getClientDetails() {
    return {
      Browser_Name: name,
      Browser_Version: version,
      Screen_Width: screen.width,
      Screen_Height: screen.height,
    };
  }
  return getClientDetails;
}
function screenResized() {
  let clientData2 = getclientDetails();
  document.querySelector("#Screen_Width").textContent =
    clientData2.Screen_Width;
  document.querySelector("#Screen_Height").textContent =
    clientData2.Screen_Height;
}
window.onresize = screenResized;
