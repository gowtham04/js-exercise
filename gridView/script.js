const createCard = function (org) {
  const card = document.createElement("div");
  const card_img = document.createElement("img");
  card_img.src = org.imgUrl;
  card_img.classList.add("img-thumbnail");
  card_img.style.background = "transparent";
  card.setAttribute("data-org", org.orgName);
  card.setAttribute("data-ceo", org.ceo);
  card.setAttribute("data-founded", org.startYear);
  card.classList.add("card");
  card.appendChild(card_img);
  return card;
};
const orgs = [
  {
    orgName: "Zoho",
    imgUrl: "orgImages/zoho.png",
    ceo: "sridhar Vembu",
    startYear: "17 March 1996; 25 years ago",
  },
  {
    orgName: "Apple",
    imgUrl: "orgImages/apple.png",
    ceo: "Tim Cook",
    startYear: "April 1, 1976; 45 years ago",
  },
  {
    orgName: "ZOHO",
    imgUrl: "orgImages/tesla.png",
    ceo: "Elon musk",
    startYear: "July 1, 2003; 17 years ago",
  },
  {
    orgName: "Alibaba",
    imgUrl: "orgImages/alibaba.png",
    ceo: "Daniel Zhang",
    startYear: "28 June 1999; 21 years ago",
  },
  {
    orgName: "Google",
    imgUrl: "orgImages/google.png",
    ceo: "Sundar Pichai",
    startYear: "September 4, 1998; 22 years ago",
  },
  {
    orgName: "Xiaomi",
    imgUrl: "orgImages/mi.png",
    ceo: "Lei Jun",
    startYear: "6 April 2010; 11 years ago",
  },
  {
    orgName: "Amazon",
    imgUrl: "orgImages/amazon.png",
    ceo: "Jeff Bezos",
    startYear: "July 5, 1994; 26 years ago",
  },
  {
    orgName: "Tata Motors",
    imgUrl: "orgImages/tata.png",
    ceo: "Guenter Butschek",
    startYear: "1945; 76 years ago",
  },
  {
    orgName: "FaceBook",
    imgUrl: "orgImages/fb.png",
    ceo: "Mark Zuckerberg",
    startYear: "February 4, 2004; 17 years ago",
  },

  {
    orgName: "Microsoft",
    imgUrl: "orgImages/microsoft.png",
    ceo: "Satya_Nadella",
    startYear: "April 4, 1975; 46 years ago",
  },
];
(function renderCards() {
  for (const org of orgs) {
    document.querySelector(".orgContainer").appendChild(createCard(org));
  }
  document
    .querySelector(".orgContainer")
    .addEventListener("mousedown", function () {
      let dataset = event.target.parentNode.dataset;
      if (!("org" in dataset)) return;

      document.querySelector("#ceo").textContent = dataset.ceo;
      document.querySelector("#startYear").textContent = dataset.founded;
      document.querySelector("#orgName").textContent = dataset.org;
      document.querySelector("#mouseClicked").textContent = true;
      document.querySelector(
        "#coOrdinates"
      ).textContent = `${event.screenX},${event.screenY}`;
    });
  document
    .querySelector(".orgContainer")
    .addEventListener("mouseup", function () {
      document.querySelector("#mouseClicked").textContent = false;
    });
})();
