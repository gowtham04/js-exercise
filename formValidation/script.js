let validations = {
  userNameValidator: function (userName) {
    return userName.trim() == "" || !userName.match(/^[a-zA-Z\s]*$/);
  },
  monthValidator: function (month) {
    return month <= 0 || month > 12;
  },
  dateValidator: function (date) {
    return date <= 0 || date > 31;
  },
  yearValidator: function (year) {
    return year < 1;
  },
  dobValidator: function (month, date, year) {
    var invalidMonth;
    var invalidDate;
    var invalidYear;
    if (month) invalidMonth = this.monthValidator(month);
    if (date) invalidDate = this.dateValidator(date);
    if (year) invalidYear = this.yearValidator(year);
    return invalidMonth || invalidYear || invalidDate;
  },
  genderValidator: function (gender) {
    return gender == "";
  },
  phoneValidator: function (phone) {
    return phone.length != 10;
  },
};
function doValidation(elemId, elemVal) {
  let validationFailed = false;
  let errorElem = document.querySelector(`#${elemId}ErrorMessage`);
  if (elemId == "userName") {
    validationFailed = validations.userNameValidator(elemVal);
  } else if (elemId == "month") {
    errorElem = document.querySelector(`#dobErrorMessage`);
    validationFailed = validations.dobValidator(elemVal);
  } else if (elemId == "date") {
    errorElem = document.querySelector(`#dobErrorMessage`);
    let month = document.querySelector("#month").value;
    validationFailed = validations.dobValidator(month, elemVal);
  } else if (elemId == "year") {
    errorElem = document.querySelector(`#dobErrorMessage`);
    let month = document.querySelector("#month").value;
    let date = document.querySelector("#date").value;
    validationFailed = validations.dobValidator(month, date, elemVal);
  } else if (elemId == "gender") {
    validationFailed = validations.genderValidator(elemVal);
  } else if (elemId == "phone") {
    validationFailed = validations.phoneValidator(elemVal);
  }
  if (validationFailed) {
    errorElem.classList.remove("invisible");
  } else if (errorElem && !errorElem.classList.contains("invisible")) {
    errorElem.classList.add("invisible");
  }
}
function showOutput() {
  event.preventDefault();
  let validationFailed = false;
  document.querySelectorAll(".errmsg").forEach(function (elem) {
    if (!elem.classList.contains("invisible")) validationFailed = true;
  });
  if (!validationFailed) {
    let userName = document.querySelector("#userName").value;
    let month = Number(document.querySelector("#month").value);
    let date = Number(document.querySelector("#date").value);
    let year = document.querySelector("#year").value;
    let gender = document.querySelector("#gender").value;
    let phone = document.querySelector("#phone").value;
    if (!date || !month || !year) {
      return document
        .querySelector(`#dobErrorMessage`)
        .classList.remove("invisible");
    }
    let dateString = month + "/" + date + "/" + year;
    $("#validationForm").hide();
    $("#_userName").text(userName);
    $("#_dob").text(dateString);
    $("#_gender").text(gender);
    $("#_phone").text(phone);
    $(".output").show();
  }
}

(function initialLoad() {
  document.querySelector("#phone").addEventListener("keypress", function () {
    if (
      this.value.length >= 10 ||
      event.key == "e" ||
      event.key == "+" ||
      event.key == "-"
    ) {
      event.preventDefault();
    }
  });
  let inputsForValidation = document.querySelectorAll(".form-control");
  for (let input of inputsForValidation) {
    input.addEventListener("focusout", function () {
      doValidation(this.id, this.value);
    });
  }
})();
