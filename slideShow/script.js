(function addUtilityFunctions() {
  Node.prototype.removeLastChild = function () {
    this.removeChild(this.children[this.children.length - 1]);
  };
  Node.prototype.removeFirstChild = function () {
    this.removeChild(this.children[0]);
  };
})();

let sliderImages = [
  "newImages/img1.jpg",
  "newImages/img2.jpg",
  "newImages/img3.jpg",
  "newImages/img4.jpg",
  "newImages/img5.jpg",
  "newImages/img6.jpg",
  "newImages/img7.jpg",
  "newImages/img8.jpg",
  "newImages/img9.jpg",
  "newImages/img10.jpg",
];

class Slider {
  constructor(sliderImages, imageSize) {
    this.sliderImages = sliderImages;
    this.imageSize = "900px";
    this.slideImagesSize = sliderImages.length - 1;
    this.firstClone = this.slideImagesSize;
    this.lastClone = 0;
    this.createSlider(sliderImages);
  }
  createSlider(images) {
    const container = document.querySelector(".imageContainer");
    for (let image of images) {
      container.appendChild(this.createSlide(image));
    }
    const slides = document.querySelectorAll(".slide");
    container.insertBefore(
      this.createSlide(images[this.firstClone]),
      slides[0]
    );
    container.appendChild(this.createSlide(images[this.lastClone]));
    this.addControls(this);
  }
  createSlide(imageUrl) {
    const img = document.createElement("img");
    img.classList.add("slide");
    img.src = imageUrl;
    img.style.width = this.imageSize;
    return img;
  }
  nextSlide() {
    const container = document.querySelector(".imageContainer");
    const slides = document.querySelectorAll(".slide");
    this.lastClone++;
    this.firstClone++;
    if (this.lastClone >= this.sliderImages.length) this.lastClone = 0;
    if (this.firstClone >= this.sliderImages.length) this.firstClone = 0;
    container.removeFirstChild();
    container.appendChild(this.createSlide(this.sliderImages[this.lastClone]));
    slides[2].style.left = "0px";
    slides[1].style.left = `-${this.imageSize} `;
  }
  previousSlide() {
    const slides = document.querySelectorAll(".slide");
    const container = document.querySelector(".imageContainer");
    console.log(this);
    this.firstClone--;
    this.lastClone--;
    if (this.firstClone < 0) this.firstClone = this.slideImagesSize;
    if (this.lastClone < 0) this.lastClone = this.slideImagesSize;
    container.removeLastChild();
    container.insertBefore(
      this.createSlide(this.sliderImages[this.firstClone]),
      slides[0]
    );
    slides[1].style.left = this.imageSize;
    slides[0].style.left = "0px";
  }
  addControls(slider) {
    document.addEventListener("keydown", function () {
      if (event.keyCode == 38 || event.keyCode == 33) slider.nextSlide();
      else if (event.keyCode == 40 || event.keyCode == 34)
        slider.previousSlide();
    });
    document.addEventListener("mousedown", function () {
      document.addEventListener("contextmenu", (event) =>
        event.preventDefault()
      );
      if (event.button == 2) slider.previousSlide();
      else if (event.button == 0) slider.nextSlide();
    });
  }
}

let mySlider = new Slider(sliderImages);
