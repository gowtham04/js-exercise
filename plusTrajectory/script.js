let wrapper = document.querySelector(".innerWrapper");
let moveImg = document.querySelector(".movingImg");
let imgSize = 120;
let wrapperWidth = wrapper.offsetWidth - imgSize;
let wrapperHeight = wrapper.offsetHeight - imgSize;
let centerWidth = wrapperWidth / 2 - 50;
let centerHeight = wrapperHeight / 2 - 50;
moveImg.style.top = centerHeight + "px";
let control;
let lastMovement = leftToCenter;
let state = false;
let moveMents = [
  leftToCenter,
  centerToRight,
  rightToCenter,
  centerToBottom,
  bottomToCenter,
  centerToTop,
  topToCenter,
  centerToLeft,
];
document.querySelector("#pause").addEventListener("click", pause);
document.querySelector("#reset").addEventListener("click", reset);
window.addEventListener("resize", () => {
  moveImg.style.left = 0;
  moveImg.style.top = centerHeight + "px";
  wrapperWidth = wrapper.offsetWidth - imgSize;
  wrapperHeight = wrapper.offsetHeight - imgSize;
  centerWidth = wrapperWidth / 2 - 50;
  centerHeight = wrapperHeight / 2 - 50;
  clearTimeout(control);
  if (state) {
    moveMents[0](moveMents);
  }
});
