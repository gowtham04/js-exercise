let newRomans = {
  1: "I",
  5: "V",
  9: "IX",
  10: "X",
  50: "L",
  90: "XC",
  100: "C",
  500: "D",
  900: "CM",
  1000: "M",
  5000: "^V",
  9000: "^I^X",
  10000: "^X",
  50000: "^L",
  90000: "^X^C",
  100000: "^C",
  500000: "^D",
  900000: "^C^M",
  1000000: "^M",
};

function convertNumDigitRoman(num) {
  let roman = "";
  let len = num.length;
  num = Number(num);
  if (!num) return "";
  else if (num > 1000000) return "limit Error";
  let startDigit = Number("1".padEnd(len, "0"));
  let middleDigit = startDigit + "0" - "5".padEnd(len, "0");
  let lastDigit = startDigit + "0" - startDigit;
  let maxDigit = startDigit + lastDigit;
  let temp = num % startDigit;
  num = num - temp;
  if (num < middleDigit) {
    roman += newRomans[startDigit];
    if (num >= middleDigit - startDigit) roman += newRomans[middleDigit];
    else roman += convertNumDigitRoman(String(num - startDigit));
  } else if (num <= lastDigit - startDigit) {
    roman +=
      newRomans[middleDigit] + convertNumDigitRoman(String(num - middleDigit));
  } else roman += newRomans[lastDigit];
  if (num >= 10) {
    roman += convertNumDigitRoman(String(temp));
  }
  return roman;
}

(function initialLoad() {
  document
    .querySelector(".form-control")
    .addEventListener("keydown", function () {
      let num = this.value;
      if (/^\d+$/.test(event.key)) {
        num += event.key;
      } else if (event.key == "Backspace") {
        num = num.slice(0, -1);
      }
      if (
        event.key == "e" ||
        event.key == "+" ||
        event.key == "-" ||
        num > 1000000
      ) {
        return event.preventDefault();
      }
      document.querySelector(".outPut").textContent = convertNumDigitRoman(num);
    });
})();
