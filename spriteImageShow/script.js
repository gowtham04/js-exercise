let spriteData = {
  //row : no.of column
  1: 8,
  2: 8,
  3: 10,
  4: 9,
  5: 7,
  6: 14,
};
let addColumns = function (row) {
  document.querySelector("#columnSelect").innerHTML = "";
  let columnCount = spriteData[row];
  for (let i = 1; i <= columnCount; i++) {
    let option = document.createElement("option");
    option.textContent = i;
    document.querySelector("#columnSelect").appendChild(option);
  }
  changeBg(row, 1);
};
let changeBg = function (row, col) {
  let colVal = -(col - 1) * 80 + "px";
  let rowVal = -(row - 1) * 58 + "px";
  document.querySelector(
    ".output"
  ).style.background = `url('sprite.png') ${colVal} ${rowVal}`;
};
(function () {
  Object.entries(spriteData).forEach((element) => {
    let option = document.createElement("option");
    option.textContent = element[0];
    document.querySelector("#rowSelect").appendChild(option);
  });
  addColumns(1);
})();

document.querySelector("#rowSelect").addEventListener("change", function () {
  addColumns(this.value);
});
document.querySelector("#columnSelect").addEventListener("change", function () {
  changeBg(document.querySelector("#rowSelect").value, this.value);
});
